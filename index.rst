
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/racism.rss
.. https://framapiaf.org/web/tags/jjr.rss
.. https://framapiaf.org/web/tags/oraaj.rss

.. index::
   pair: ORAAJ ; Organisation Revolutionnaire Antiraciste Antipatriarcale Juive
   ! ORAAJ


|FluxWeb| `RSS <http://oraaj.frama.io/oraaj-info/rss.xml>`_

.. _oraaj:

====================================================================================
|oraaj| ORAAJ **Organisation Revolutionnaire Antiraciste Antipatriarcale Juive**
====================================================================================

- https://www.instagram.com/oraaj___/
- https://blogs.mediapart.fr/oraaj
- :ref:`antisem:oraaj`

Adresse mail: oraajuive@gmail.com


.. toctree::
   :maxdepth: 5

   actions/actions
