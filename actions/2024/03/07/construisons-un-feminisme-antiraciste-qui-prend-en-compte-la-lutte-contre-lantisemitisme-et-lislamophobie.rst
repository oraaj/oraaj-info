.. index::
   ! Construisons un féminisme antiraciste qui prend en compte la lutte contre l’antisémitisme et l’islamophobie (2024-03-07)

.. _oraaj_2024_03_07:

===================================================================================================================================================================================================================
|oraaj| 2024-03-07 **Construisons un féminisme antiraciste qui prend en compte la lutte contre l’antisémitisme et l’islamophobie** par ORAAJ (Organisation Révolutionnaire Antiraciste Antipatriarcale Juive)
===================================================================================================================================================================================================================


Construisons un féminisme antiraciste qui prend en compte la lutte contre l’antisémitisme et l’islamophobie
===============================================================================================================

Nous, collectif féministe antiraciste de lutte contre l'antisémitisme, avons
envie de vomir de l'état critique actuel du mouvement féministe.

**Les féministes de gauche, qui composent notre camp politique**, sont encore une
fois prises dans un jeu dangereux pour nous, juifves, mais aussi pour la lutte
féministe et antiraciste : **celui de la complaisance avec l’antisémitisme**.

Tandis que les féministes réactionnaires sont prises dans des confusions entre
lutte contre l'antisémitisme et défense de la politique israélienne.

Dans un contexte français, où on a lu des communiqués de collectifs et partis
en soutien entier à toute forme de résistances en Palestine, y compris celle
du Hamas ; dans un contexte où on entend une partie de la droite et au delà
soutenir les massacres à Gaza et les violences de la colonisation, et réprimer
tout soutien à la Palestine ; n'est-il pas temps de déployer des positions
politiques plus complètes ?

**Le féminisme décolonial ne doit pas fantasmer une résistance sexiste et antisémite,
et la lutte contre l'antisémitisme doit aussi lutter contre la déshumanisation
et l'invisibilisation des palestinien.nes !**

Dans une histoire de la gauche, qui a nié à l'époque le caractère antisémite du
meurtre d'Ilan Halimi, qui ne s’est pas emparée politiquement des attentats
antisémites, ni même des féminicides et agressions sexistes antisémites
en France ces dernières années, nous ne sommes pas étonné.es de ces
confusions.

Et puisque le fonctionnement de l'antisémitisme semble incompris, nous nous
devons de le définir dans cette situation : le 25 novembre 2023, pour la marche
contre les violences sexistes et sexuelles, c'était considérer que des femmes
victimes de telles violences et tuées parce que juives, n'ont pas le droit
à notre égard féministe car elles sont israéliennes.

**Quand on abandonne des femmes pour leur identité, c'est ce qu'on appelle du racisme.**

C'est ainsi que dans la tribune `Propagande de guerre pro-israélienne : notre
féminisme ne se laissera pas enrôler ! 2023-11-23 <https://www.lemediatv.fr/articles/2023/propagande-de-guerre-pro-israelienne-notre-feminisme-ne-se-laissera-pas-enroler-i7Hc1eCST7miT1iGrjVL3A>`_ ,
vous dites que le signifiant antisémite est mal utilisé : c'est vous qui en faites une coquille
vide à force de l'exclure de vos champs d'analyses et de lutte !

Toutes vos connaissances scientifiques et empiriques en terme d'analyse du
monde social ne sont malheureusement pas mobilisées dans cette tribune, qui
nie l'expérience raciale de la violence.

Ne pas vouloir mobilise le concept de "féminicides de masse" n'empêche pas
de parler des violences sexistes et sexuelles perpétrées sur des femmes juives,
et une dénonciation sans réserve n'affaiblit pas une analyse du contexte
politique.

C'est vous qui projetez un "Orient monstrueux" (selon vos mots) en opposant
les femmes palestiniennes et israéliennes selon leurs souffrances et leurs
positionnements dans les rapports sociaux de race : ces femmes font toutes
parti de "l'orient" que vous méjugez et fantasmez.

De l'autre côté de l'échiquier politique, la tribune `"Pour la reconnaissance
d’un féminicide de masse en Israël le 7 octobre" (2023-11-10) <https://www.liberation.fr/idees-et-debats/tribunes/pour-la-reconnaissance-dun-feminicide-de-masse-en-israel-le-7-octobre-20231110_EMTPN3H2EBDLJBMLLTZ2SRLY6A/?redirected=1>`_ fait une
grave erreur en disant qu'on peut parler des violences perpétrées par le Hamas
sans analyser le contexte géopolitique.

Il faut parler de l’étendue de la terreur qui s'abat sur les femmes gazaouies,
qui subissent depuis trop longtemps les effets de la colonisation sur les
rapports de genre et sur les violences sexistes et sexuelles.

Les mouvements qui se disent "humanistes" et qui emploient des modes d'action
symboliques tels que les taches de sang à l'entre-jambe, semblent aujourd'hui
incapables de parler de la souffrance des palestinien.nes, car ils sont aveuglés
par leur islamophobie et alimente ainsi la réception de la propagande de
l'extrême droite au pouvoir en Israël.

La lutte contre l'antisémitisme, ainsi mobilisée par des forces réactionnaires,
participe à alimenter d'autres racismes.

Nous, Oraaj (Organisation Revolutionnaire Antiraciste Antipatriarcale Juive)
sommes sur une ligne de crête, nous tentons de nous rattacher à des AG féministes
et décoloniales pour la Palestine, nous tentons de lutter contre l'antisémitisme
en même temps, nous tentons de co-construire, tandis que les analyses et
rhétoriques antisémites se déploient encore dans les mouvements militants de
gauche et d'extrême gauche, et tandis que la répression politique s'abat sur
les mobilisations pour la Palestine.

**Nous nous retrouvons alors isolé.es à la fois politiquement mais aussi physiquement**.

Dans cette manifestation de samedi 25 novembre 2023, nous n'avons pas arrêté
de nous déplacer pour trouver une place où défiler ; une partie du mouvement
féministe est sourde face à nos critiques, une autre tente de discréditer nos
dénonciations d'antisémitisme, et ce par plusieurs techniques de décrédibilisation.

- La première serait qu'il n'y aurait qu'une instrumentalisation de l'antisémitisme.
- La deuxième que nous ferions la confusion entre antisémitisme et antisionisme.
  La dernière étant de nous qualifier de sioniste.

Nous pensons que parler d’antisémitisme uniquement par le biais de son
instrumentalisation est une erreur d'analyse, et participe à l'invisibilisation
de l’antisémitisme et donc à sa reproduction.

Nous pensons que l'antisionisme n'est pas forcément de l'antisémitisme, mais
que certaines positions antisionistes sont formées de rhétoriques antisémites.

Nous pensons qu'il y a des sionismes de droite et d'extrême droite, comme
celui d'Israël, qui sont des idéologies racialistes et coloniales.

Et qu'il y a des sionismes et antisionismes auxquels il faut se référer pour
penser l'existence des juifs et palestinien.nes sur un territoire commun.

La question du sionisme et de l’antisionisme prise de manière essentialiste
et réductrice, de tous les côtés, ne fait qu'envenimer la situation politique.

Ces définitions identitaires sont inopérantes quand elles sont mobilisées
pour classer les "bons" et "mauvais" juifs et les "bons" et "mauvais" militants.

**Let us tell our stories !** Celles de différentes oppositions à la colonisation
et à la politique d’Israël, avec des perspectives multiples liées aux
parcours d'exils et de migrations, et aux stratégies d'auto-défense face à
un antisémitisme historique, et toujours opérant au présent.

Vous pensez qu'il n'y a symboliquement pas de victimes collatérales de ces
positionnements polarisés ? Eh bien vous vous trompez.

Jamais nous ne nous allierons avec les féministes de droite et d'extrême droite,
**nous voulons construire une lutte féministe de gauche contre l'antisémitisme
et pour Gaza !**

**Nous nous adressons à notre camp politique et l'enjoignons à faire preuve
d'auto-critique, de féminisme antiraciste et donc contre l'antisémitisme !**


Commentaires de Jeanne
============================

- http://www.ynetnews.com/articles/0,7340,L-3786802,00.html
- http://www.theguardian.com/world/2009/oct/18/hamas-gaza-islamist-dress-code
- http://www.theguardian.com/world/2010/mar/04/hamas-ban-men-gaza-salons
- http://www.thenational.ae/news/world/middle-east/hamas-ban-on-mixed-sex-schools-in-gaza-comes-into-force

Merci Milena pour ton texte qui s'empare de la complexité du sujet.

En complément, j'aimerais poser une question et souligner d'autres lignes
de crête.  Comment lutter contre l'antisémitisme en 2024 et après le 7/10
alors que l'antisémitisme est depuis longtemps au coeur de la propagande du
Hamas, du Quatar et d'Al-Jazeera en langue arabe et diffusée en France ?

Alors quelques remarques sur ton texte: - "Parler de l'étendue de la terreur
qui s'abat sur les femmes gazaouies qui subissent depuis trop longtemps les
effets de la colonisation...", me semble être une imprécision risquée.
A ce jour, les femmes gazaouis sont victimes du Hamas qui a imposé la Charia
à Gaza dans les années 2010 avec ce type de mesures (je mets qq références
plus bas): interdiction aux motcyclistes de conduire une femme sur leur siege
arrière, obligation de porter des abayas style frêres musulmans, interdiction
des écoles mixtes à partir de 9 ans etc.... Elles sont aussi victimes des
bombardements israéliens comme tous les habitants de Gaza mais pas en tant
que femmes. Gaza n'est pas une colonie israélienne même si cela peut être
aujourd'hui un objectif de certains extrémistes israéliens, on n'y est pas.

Je trouve intéressant que l'on n'ose plus parler de sionisme de gauche (je
fais référence à ta phrase sur les "sionismes et antisionismes auxquels se
référer").

Pour me le remémorer j'ai ouvert le livre de Thomas Vescovi,
L'Echec d'une Utopie, et je suis tombée sur une référence à Moses Hess
(proche de Karl Marx) et son livre, Rome et Jérusalem, publié en 1862.

Pour lui, l'assimilation ne résoudrait pas le problème de l'antisémitisme et il
préconisait la création d'un Etat pour les juifs.

J'ai l'impression qu'aujourd'hui, nous pensons ou pouvons imaginer que
l'effacement d'Israel d'une façon ou d'une autre résoudrait le problème de
l'antisémitisme dans le monde en même temps qu'il résoudrait les mefaits
de la colonisation des derniers siècles.

Appelons un chat, un chat, lutter contre l'antisémitisme aujourd'hui en 2024,
c'est aussi lutter contre la démonisation d'Israël et placer historiquement
Israël dans l'histoire du colonialisme sans en faire (à tort) l'archetype
de l'Etat colonial. Pour cela, nous devons à chaque fois préciser les
complexités historiques et géographiques des frontières d'Israël,de
l'Autorité palestinienne, de la bande de Gaza et de la Cisjordanie occupée.

C'est dur, il nous manque des utopies et la flamme des guerrières de la Paix
semble bien pâle face à tout ce brouhaha.

Jeanne

- `“Gaza : Hamas bans motorbike rides for women” <http://www.ynetnews.com/articles/0,7340,L-3786802,00.html>`_ ,
  Associated Press, 10 juillet 2009.
- `“Hamas patrols beaches in Gaza to enforce conservative dress code” <http://www.theguardian.com/world/2009/oct/18/hamas-gaza-islamist-dress-code>`_,
  par Rory McCarthy, The Guardian, 18 octobre 2009.
- `“Hamas bans men from working in female hair salons” <http://www.theguardian.com/world/2010/mar/04/hamas-ban-men-gaza-salons>`_, Associated Press, 4 mars 2010.
- `« Hamas ban on mixed-sex schools in Gaza comes into force » <http://www.thenational.ae/news/world/middle-east/hamas-ban-on-mixed-sex-schools-in-gaza-comes-into-force>`_, AFP,
  2 avril 2013.

