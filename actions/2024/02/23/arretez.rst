.. index::
   ! Arrêtez (2024-02-23)

.. _oraaj_2024_02_23:

=====================================================================================================================================
2024-02-23 **Arrêtez** par ORAAJ (Organisation Révolutionnaire Antiraciste Antipatriarcale Juive)
=====================================================================================================================================

- https://blogs.mediapart.fr/oraaj/blog/230224/arretez

:download:`Télécharger le PDF <pdfs/oraaj_tribune_antisem_mvt_feministes_2024_03_07.pdf>`

**Arrêtez**
==============

Cet appel fait suite à plusieurs commentaires de nos publications et autres
"accusations" en ligne.  depuis plusieurs semaines, nous subissons une pression
grandissante à nous justifier sur nos positions concernant israël/palestine.

Nous rappelons qu’oraaj (et avant juivnr) n’a pas attendu le 7 octobre 2023
pour exister.
Nos lignes antiracistes et feministes se construisent sur de nombreux sujets,
à travers le temps et au fil de la constitution du collectif.

Pour l’heure donc : **arretez de nous "traiter de sionistes"**

**Arretez de nous "traiter de sionistes"** comme si on justifiait les actes
d’israel contre les palestinien.nes, ce que nous n’avons jamais fait et
ne ferons jamais.

**Arretez** de mettre indistinctement des orgas juives dans un meme sac
"sioniste" alors qu’elles n’ont  pas les memes lignes politiques :
**c’est essentialiser et confondre les juifs**.

**Arretez** de dire que si on démonte le hamas, ses fondements, ses actes et ses
vss, ça veut dire qu’on soutient israel.

**Arretez** de nous faire croire que parler de l’antisemitisme c’est affaiblir
la lutte decoloniale.

**Arretez** de nous suspecter du pire  sous pretexte qu’on adopte pas des
positions de **"jewish guilt"**, qu’on essaye de défendre des lignes
politiques et de ne pas devenir des tokens (les tokens, vous le savez bien,
n’aidant generalement en rien à faire politiquement bouger les choses).

**arretez de nous "traiter de sionistes"** sous pretexte qu’on pose la
question de comment les juifves peuvent vivre dans le monde.

**Faites confiance à vos camarades juifves** qui s’evertuent à trouver des
positions qui correspondent à leurs réalités situées et multiples.

Notre critique du racisme et de la politique coloniale de l’etat
israelien chaque jour plus décomplexée est sévère, notre coeur est avec les
palestinien.nes, nos pensées vont aussi à toutes les forces anti occupation
en israël, tandis que notre esprit est ici à dealer avec une histoire
antisémite et raciste bien trop vivace dans toutes les ideologies digérées
par les personnes en france.
