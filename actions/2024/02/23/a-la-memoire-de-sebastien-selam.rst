.. index::
   pair: Sébastien Selam ; A la mémoire de Sébastien Selam (2024-02-23)

.. _oraaj_seleam_2024_02_23:

=====================================================================================================================================
2024-02-23 **A la mémoire de Sébastien Selam**
=====================================================================================================================================

- https://blogs.mediapart.fr/oraaj/blog/230224/la-memoire-de-sebastien-selam

2023 était la vingtième année du meurtre, le 20 novembre 2003, de Sébastien
Selam, 23 ans, assassiné parce que juif.

L'auteur du meurtre, son ami d'enfance et voisin, sera reconnu irresponsable
pénalement en 2010, la justice laissant le caractère antisémite de ce crime
derrière elle.
Il avait pourtant déclaré "J'ai tué un Juif ! J'irai au paradis", et après
son arrestation "s'il est mort, je suis trop content, ce putain de juif, sale juif".

C'était la même année, en février 2003, qu'était passée la loi
prévoyant la circonstance aggravante "à raison de l'appartenance ou de
la non-appartenance, de la victime à une ethnie, une nation, une race ou une
religion déterminée".
Celle-ci ne sera pas mobilisée, et ne l'a que trop peu encore aujourd'hui
dans les crimes racistes, sexistes, trans et homophobes.

Ce meurtre sera tristement récupéré par Zemmour, dans une fiction dans
laquelle il s'inspire de la vie et la mort de Sébastien Selam : **les parents
de Sébastien se battent encore aujourd'hui contre cette récupération**.

**Par contre, l'antiracisme ne s'est jamais saisi de ce meurtre**.

**Nous voulons que vingt ans après, cette histoire soit connue de nos camarades
de lutte** et soit reconnue comme un meurtre raciste, y compris car la famille
Selam attend toujours que ce soit le cas.

La famille n'a pas déménagé, elle vit toujours dans le même immeuble, le
même appartement du 19e arrondissement de Paris, un quartier où les conflits
intercommunautaires ont une longue histoire.

Lors de son procès plus tard, l'auteur déclarera : "J'ai entendu des
voix dans ma tête. Je croyais que j'étais mort."
Nous ne voulons plus que la justice fonctionne à la négative.

Nous voulons qu'elle soit capable de nommer le caractère raciste de tels crimes,
tout en étant capable de prendre en charge les soins psys nécessaires à la
santé mentale des auteurices.

Nous voulons plus de moyens et de meilleurs suivis psys, et nous nous opposons
à toutes les politiques carcérales inhumaines.

Nous voulons que cette prise en charge aille de pair avec la reconnaissance
des caractères antisémites et racistes des actes pénaux ou criminels, y
compris ceux de la police qui est encore moins inquiétée par la justice - comme
nous l'avons encore vu avec le procès de Théo en janvier 2024 : pour que plus
personne ne soit agressé ou ne meurt parce que racisé.

Pour cela nous pensons qu'il faut plus de formation contre l’antisémitisme et
contre tous les racismes dans les écoles, dans les quartiers, dans les espaces
communautaires et religieux, et pour cela il faut commencer par reconnaitre
les histoires de racialisations et les histoires coloniales de notre pays,
et leurs effets sur notre société.

La mère du meilleur ami de Sébastien Selam enlevait déjà les mezouzas
dans son immeuble, elle avait insulté un rabbin dans le quartier, **elle a
transmis des croyances antisémites à son fils qui se sont exprimées sous
forme meurtrière dans un moment de crise psy**.

Ce meurtre est arrivé dans la période de la 2ème Intifada en Palestine et
Israël, ces moments de crises au proche orient où l'on sait qu'ici en France,
ce sont des moments de flambées de l'antisémitisme et de l'islamophobie.

La manière dont l'état et les instances de représentativité institutionnelles
voilent ces réalités et nient leurs racismes ne fait qu'aggraver la situation:
à l'époque, en 2003, une élue de la mairie de Paris s'était déplacée
sur la scène du crime le jour-même pour neutraliser et nier le caractère
systémique du meurtre.

Aujourd'hui, dans notre contexte actuel de flambée de l'antisémitisme, si ne
nous sommes pas capables de prendre en compte et en charge tous les racismes
dans nos camps politiques, nous nous exposons à rejouer éternellement les
mêmes histoires.

Pour Sébastien et toutes les victimes de meurtres racistes, nous demandons
la reconnaissance des crimes de haine raciale et une mobilisation antiraciste
pour toutes les minorités.
