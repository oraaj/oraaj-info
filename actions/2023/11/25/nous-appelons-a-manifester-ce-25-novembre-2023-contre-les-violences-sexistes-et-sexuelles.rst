.. index::
   pair: Manifestation ; 2023-11-25
   pair: Hommage ; Tal Pieterbraut-Merx

.. _oraaj_2023_11_25:

=========================================================================================================================
|oraaj| 2023-11-25 **Nous appelons à manifester ce 25 novembre 2023 contre les violences sexistes et sexuelles**
=========================================================================================================================

- https://www.instagram.com/p/C0BpC-ZL2C-/?utm_source=ig_web_copy_link&igshid=MzRlODBiNWFlZA==


Nous appelons à manifester ce 25 novembre 2023 contre les violences
sexistes et sexuelles : rendez-vous à  13h45 devant le Royal Nation 

Ecrivez nous (oraajuive@gmail.com) pour signer l'appel si vous le souhaitez.  

Nous appelons à rejoindre la manifestation, comme tous les ans, pour s'élever 
contre les violences sexistes et sexuelles, les féminicides et les meurtres 
des personnes trans, qui, construits dans le système de genre et patriarcal 
dans lequel nous vivons, sont produits par différentes instances et s'expriment
dans différentes sphères de nos vies : 

- les violences domestiques,
- l'hétérosexualité, 
- les violences scolaires, 
- celles envers les enfants,
- les violences d'état...  

Ces violences sont souvent indissociables des violences raciales et sociales, 
et c'est pourquoi notre lutte féministe ne va pas sans une lutte antiraciste *
et anticapitaliste.  

Au travail, les violences sexistes et sexuelles sont renforcées par les rapports
de hiérarchies : les politiques mises en place par le gouvernement de casse syndicale, 
ainsi que la précarisation, notamment des femmes et minorités de genre racisées, 
assignées à des métiers dévalorisés, renforcent les situations de violences 
et de harcèlements.  

Le système de domination adulte rend les violences contre les enfants structurelles,
et comme le disait Tal Pieterbraux-Mert, il faut urgemment intégrer ces
violences à "la grande famille des rapports de pouvoir".  

Les violences sexistes et sexuelles, ainsi que les discriminations, envers les personnes
LGBTQI sont particulièrement fortes. 

Les politiques de washing nous divisent : jamais l'émancipation des minorités 
de genre ne passera par des fausses promesses ou des injonctions à représenter 
une minorité modèle de genre "progressiste".  

L'auto-détermination des minorités de genre est la seule voie vers la lutte 
féministe.  

Les violences sexistes et sexuelles sont renforcées

- par les nationalismes et les extrémismes qui mettent en danger les vies de 
  personnes sexisé.es, 
- par les  politiques fémonationalistes qui produisent du racisme et 
  particulièrement  de l'islamophobie en instrumentalisant le féminisme,
- par la négrophobie coloniale occidentale qui touche particulièrement
  les femmes noires, par la fétichisation des noir.es, arabes, asiatiques,
  juifves, 
- ou encore par les violences religieuses et politiques faites
  à nos camarades iraniennes...  
  
Dans le contexte actuel 

**nous pensons aux israélien.nes qui ont été victimes de violences sexuelles 
et de meurtres le 7 octobre 2023**.

**Nous pensons aux palestinien.nes, tuées chaque jour de guerre, qui subissent 
depuis trop longtemps les conséquences de la colonisation et ce qu'elles 
engendrent en terme de rapport et de violence de genre**.  

Ce contexte donne lieu à une augmentation des actes antisémites et islamophobes, 
y compris à caractère sexiste. 

Les meurtres à caractère sexiste-raciste, y compris à caractère sexiste-antisémite,
et notamment les féminicides antisémites, ne sont encore que trop peu reconnus, 
par la justice mais aussi dans notre camp politique. 

Nous nous opposons à des formes de féminismes qui instrumentalisent un certain type
de féminisme à des fin racistes, antisémites, ou transphobes. 

Et nous voulons construire un mouvement féministe et afroféministe antiraciste
qui soit capable d'instaurer un rapport de force.  

Nous voulons aussi marcher contre les violences sexistes, quasi ordinaires, 
subies dans les milieux militants. 

Nous voulons militer dans tous les espaces sans peur des retours de bâtons ou 
des silenciations. 

Nous rappelons toutes les fois où dénoncer le sexisme dans des collectifs est inaudible,
toutes les fois où les voix qui se lèvent sont tues sous prétexte de la noblesse 
ou l'urgence de causes militantes, toutes les fois où les personnes dominantes 
sont laissées à des positions de pouvoir pour leur soi-disant capacité à créer 
l'adhésion quitte à fermer les yeux sur leurs agissements sexistes. 

Toutes les fois où parler d'agressions dans les collectifs provoque un lever 
de bouclier de complaisance, rendant encore plus seul.es les victimes.  

Nous rendons hommage aux victimes de meurtres sexistes et/ou racistes cette année, 
aux personnes trans mortes dont ont été rappelées les mémoires cette semaine 
lors du Tdor, aux travailleur.euses du sexe violentées et tuées chaque année, 
aux enfants victimes de violences, et à notre camarade Tal Pieterbraut-Merx.

